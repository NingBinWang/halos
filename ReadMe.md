# HalOS
Halos主要的思想来自于嵌入式的HAL库的模式，理想是在虚拟架构下，提供相应的虚拟驱动，与硬件抽象层进行隔离，所有的内核与启动仍使用原生的。

此处的编译框架主要借鉴

# 编译依赖


## 源码目录结构

|  目录名	     |   描述                                |
| ----          |      ----                             |
| board         |    芯片级别的编译控制                   |
| bootloader    |    包含uboot RTOS TS OPTEE编译脚本等  |
| kernel        |    内核编译脚本                       |
| configs       |    该OS的相关配置                     |
| package       |    自研软件包                         |
| scripts       |    相关支持的脚本                     |
| filesystem    |    文件系统工具                       |
| rootfs        |    主要包含制作debin distro等rootfs      |
| applications	|    应用程序样例，包括camera等             |
| base	        |    基础软件服务子系统集&硬件服务子系统集   |
| build	        |    组件化编译、构建和配置脚本             |
| docs	        |    说明文档                              |
| domains	    |    增强软件服务子系统集                   |
| drivers	    |    驱动子系统                            |
| foundation	|    系统基础能力子系统集                   |
| third_party	|    开源第三方组件                        |
| utils	        |    常用的工具集                          |
| vendor	    |    厂商提供的软件                        |
| build.py	    |    编译脚本文件                          |
| Makefile      |     OS的编译脚本                         |

# 参考
一部分参考了内核的相关编译框架
本OS参考了