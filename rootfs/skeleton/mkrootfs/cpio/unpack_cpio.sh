#!/bin/sh
# The user id of root
ROOT_UID=0


# The one who own 'root' permission can run this script
if [ "$UID" -ne "$ROOT_UID" ];then
        echo "Must be root to run this script."
#        exit 1
fi

if [ $# -ne 2 ];then
        echo "Usage: `basename $0` <filename> <dir>"
        exit 1
fi

echo
echo "************************************"
echo "**          Unpack Cpio           **"
echo "************************************"
echo

echo "cpio name: $1"
cpio_name=$1

if [ ! -e $2 ]; then
	mkdir $2;
fi

CUR_DIR=$PWD

if [ -e $2 ];then
	cd $2;
	fakeroot cpio -idmv < $CUR_DIR/$cpio_name;
	cd -;
fi

chmod u+x $cpio_name

exit
